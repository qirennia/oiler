extern crate num;

#[macro_use]
mod util;

mod series;


fn main() {
    /*for x in series::Fibonacci::<u64>::new().take(10) {
        println!("{}", x);
    }*/

    for x in series::Primes::new().take(10) {
        println!("{}", x);
    }
}
