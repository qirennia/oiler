pub struct Primes {
    curr: u64,
}

impl Primes {
    pub fn new() -> Primes {
        Primes { curr: 1 }
    }
}

impl Iterator for Primes {
    type Item = u64;
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            self.curr += 1;
            if is_prime(self.curr) {
                return Some(self.curr);
            }
        }
    }
}

pub fn is_prime(n: u64) -> bool {
    if n < 2 { return false; }
    if n == 2 { return true; }

    for x in 2..(n/2)+1 {
        if n % x == 0 { return false; }
    }

    return true;
}
