extern crate num;

#[macro_use]
use util;

pub_trait_group!(Int = num::Integer + num::CheckedAdd + Copy);

pub struct Fibonacci <T: Int> {
    curr: T,
    next: T,
}

impl <T: Int> Fibonacci <T> {
    pub fn new() -> Fibonacci<T> {
        Fibonacci { curr: T::zero(), next: T::one() }
    }
}

impl <T: Int> Iterator for Fibonacci <T> {
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        match self.curr.checked_add(&self.next) {
            Some(sum) => {
                self.curr = self.next;
                self.next = sum;
                Some(self.curr)
            }
            None => None,
        }
    }
}
